# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header:  $

EAPI="2"

# Target configurations
__target_toolchain=cs2007q3-glibc2.5
__target_prefix=FREMANTLE
__rootstrap_prefix=maemo-sdk-rootstrap
__rootstrap_suffix=tgz

# Target configuration for armel
__armel_toolchain=${__target_toolchain}-arm7
__armel_target=${__target_prefix}_ARMEL
__armel_devkits=perl:debian-etch:qemu:doctools:svn:git
__armel_cputransp=qemu-arm-sb

# Target configuration for i386
__i386_toolchain=${__target_toolchain}-i486
__i386_target=${__target_prefix}_X86
__i386_devkits=perl:debian-etch:doctools:svn:git

SBOX_GROUP="sbox"
RESTRICT="strip binchecks mirror"

MYPV="5.0beta2"

DESCRIPTION="A scratchbox SDK for Maemo5 used by the Nokia N900."
HOMEPAGE="http://maemo.org/development/"
SRC_URI="http://repository.maemo.org/unstable/${MYPV}/i386/${__rootstrap_prefix}_${MYPV}_i386.${__rootstrap_suffix}
	http://repository.maemo.org/unstable/${MYPV}/armel/${__rootstrap_prefix}_${MYPV}_armel.${__rootstrap_suffix}"
LICENSE="GPL-2"
SLOT="5"
KEYWORDS="~x86"
IUSE=""

DEPEND=">=dev-embedded/scratchbox-1.0.14
	>=dev-embedded/scratchbox-devkit-qemu-0.10.0.0.5
	>=dev-embedded/scratchbox-devkit-debian-1.0.10
	>=dev-embedded/scratchbox-devkit-doctools-1.0.13
	>=dev-embedded/scratchbox-devkit-perl-1.0.4
	>=dev-embedded/scratchbox-devkit-apt-https-1.0.5
	>=dev-embedded/scratchbox-toolchain-cs2007q3-glibc2_5-1.0.11
	>=dev-embedded/scratchbox-devkit-svn-1.0
	>=dev-embedded/scratchbox-devkit-git-1.0.1
	x11-base/xorg-server[kdrive]"
RDEPEND="${DEPEND}"

SCRATCHBOX_DIR="/opt/scratchbox"

TARGET_DIR="${SCRATCHBOX_DIR}/packages"

S=${WORKDIR}/scratchbox/packages

setup_target() {
	local scratchbox
	local target
	local rootstrap
	local toolchain
	local devkits
	local cputransp
	scratchbox=$1
	target=$2
	rootstrap=$3
	toolchain=$4
	devkits=$5
	cputransp=$6
	einfo "Setting up '$target' target."
	# Settings string
	local setup_string
	setup_string="--compiler=$toolchain"
	if [ "x$devkits" != "x" ] ; then
		setup_string="$setup_string --devkits=$devkits"
	fi
	if [ "x$cputransp" != "x" ] ; then
		setup_string="$setup_string --cputransp=$cputransp"
	fi
	# Setup
	$scratchbox/tools/bin/sb-conf setup $target --force $setup_string
	if [ $? != 0 ] ; then
		ewarn "Scratchbox command 'sb-conf setup' returned error $?."
		die "Scratchbox command 'sb-conf setup' returned error $?."
	fi
	# Reset
	$scratchbox/tools/bin/sb-conf reset -f $target
	if [ $? != 0 ] ; then
		ewarn "Scratchbox command 'sb-conf reset' returned error $?."
		die "Scratchbox command 'sb-conf reset' returned error $?."
	fi
	# Select
	$scratchbox/tools/bin/sb-conf select $target
	if [ $? != 0 ] ; then
		ewarn "Scratchbox command 'sb-conf select' returned error $?."
		die "Scratchbox command 'sb-conf select' returned error $?."
	fi
	# Rootstrap
	$scratchbox/tools/bin/sb-conf rootstrap $target $rootstrap
	if [ $? != 0 ] ; then
		ewarn "Scratchbox command 'sb-conf rootstrap' returned error $?."
		die "Scratchbox command 'sb-conf rootstrap' returned error $?."
	fi
	# Install
	$scratchbox/tools/bin/sb-conf install $target -L --etc --devkits --fakeroot
	if [ $? != 0 ] ; then
		ewarn "Scratchbox command 'sb-conf install' returned error $?."
		die "Scratchbox command 'sb-conf install' returned error $?."
	fi
	einfo "Target '$target' has been created."
	echo
}

src_install() {
	dodir ${TARGET_DIR}
	cp -pRP * "${D}/${TARGET_DIR}"
}

pkg_postinst() {
	elog
	elog "You need to run:"
	elog "\"emerge --config =${CATEGORY}/${PF}\""
	elog "as the user you want delevop with"
	elog "to setup the scratchbox targets."
	elog
}

pkg_config() {
	if [ -d ${SCRATCHBOX_DIR}/${USER} -a -O ${SCRATCHBOX_DIR}/${USER} ]; then
		ewarn "'${USER}' is not a scrathbox user!"
		ewarn "You have to run:";
		ewarn "\"emerge --config dev-embedded/scratchbox\""
		ewarn "as root before configuring ${PF}"
		die "not a scratchbox user"
	fi

	einfo "Do you want to configure the scratchbox targets? [YES/no]"
	read choice
	echo
	case "$choice" in
		n*|N*)
			die "abort by user"
			;;
		*)
			;;
	esac

	if [ -d ${SCRATCHBOX_DIR}/users/${USER}/targets/$__armel_target -o -d ${SCRATCHBOX_DIR}/users/${USER}/targets/$__i386_target ]; then
		ewarn "Targets are already setup!"
		die "Targets are already setup!"
	fi

	# Setup and create SDK_ARMEL targets
	setup_target ${SCRATCHBOX_DIR} \
		$__armel_target \
		${__rootstrap_prefix}_${__version}_armel.${__rootstrap_suffix} \
		$__armel_toolchain \
		$__armel_devkits \
		$__armel_cputransp

	# Setup and create SDK_X86 target
	setup_target ${SCRATCHBOX_DIR} \
		$__i386_target \
		${__rootstrap_prefix}_${__version}_i386.${__rootstrap_suffix} \
		$__i386_toolchain \
		$__i386_devkits

	echo "export DEB_BUILD_OPTIONS=maemo-launcher,thumb,vfp" > ${SCRATCHBOX_DIR}/users/$USER/targets/$__armel_target.environment

	einfo
	einfo "Package Selection:"
	einfo " 1) Minimal Rootstrap only"
	einfo " 2) Runtime Environment"
	einfo " 3) Runtime Environment + All Dev Packages"
	einfo " 4) Runtime Environment + All Dev and Dbg Packages"
	einfo
	einfo "Please choose [1234]: "
	read __answer
	case $__answer in
		2)
			__installed_module=maemo-sdk-runtime
			;;
		3)
			__installed_module=maemo-sdk-dev
			;;
		4)
			__installed_module=maemo-sdk-debug
			;;
		*)
			;;
	esac

	for __update_target in $__armel_target $__i386_target ; do
		[ "x$http_proxy" != "x"  ] && echo "Acquire::http::Proxy \"$http_proxy\";" >  ${SCRATCHBOX_DIR}/users/$USER/targets/$__update_target/etc/apt/apt.conf.d/99proxy
		${SCRATCHBOX_DIR}/tools/bin/sb-conf select $__update_target
		${SCRATCHBOX_DIR}/login apt-get -o Acquire::http::TimeOut=15 -o Acquire::http::Retries=2 update
		if [ x$__installed_module != "x" ] ; then
			einfo "Installing packages on '$__update_target'."
			$__scratchbox/tools/bin/sb-conf select $__update_target
			$__scratchbox/login fakeroot apt-get install -y --force-yes -o Acquire::http::Pipeline-Depth=0 $__installed_module 2>&1
		fi
	done

	einfo
	einfo "Configuration finished."
	einfo "Use sb-menu inside the scratchbox to select your target."
	einfo "You can create a display window by runing outside the scratchbox:"
	einfo "\"Xephyr :2 -host-cursor -screen 800x480x16 -dpi 96 -ac -kb &\""
	einfo "Now set the DISPLAY variable inside the scratchbox with:"
	einfo "\"export DISPLAY=:2\""
	einfo "to be able to start the UI framework inside the scratchbox using:"
	einfo "\"af-sb-init.sh start\""
	einfo
	einfo "In order to obtain Nokia-closed binaries, visit"
	einfo "http://tablets-dev.nokia.com/eula/index.php to accept the End User"
	einfo "License Agreement. You will be given a token to access the Nokia"
	einfo "binaries repository with further instructions."
	einfo
}
